// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "ItemDataStructure.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class EItemType : uint8
{
	NONE UMETA(DisplayName = "NONE"),
	WEAPON_MAIN UMETA(DisplayName = "WEAPON_MAIN"),
	WEAPON_SUB UMETA(DisplayName = "WEAPON_SUB")
};

UENUM(BlueprintType)
enum class EItemValue : uint8
{
	COMMON UMETA(DisplayName = "COMMON"),
	UNCOMMON UMETA(DisplayName = "UNCOMMON"),
	RARE UMETA(DisplayName = "RARE"),
};


USTRUCT(Atomic, BlueprintType)
struct FItemData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()	
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName ItemName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EItemType ItemType = EItemType::NONE;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EItemValue ItemValue = EItemValue::COMMON;

};

//USTRUCT(Atomic, BlueprintType)
//struct FItemList
//{
//	GENERATED_USTRUCT_BODY()
//
//public:
//	UPROPERTY(EditAnywhere, BlueprintReadWrite)
//	TArray<FItemData> ItemArray;
//};
//
