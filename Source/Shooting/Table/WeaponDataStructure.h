﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Table/ItemDataStructure.h"
#include "WeaponDataStructure.generated.h"
/**
 * 
 */

UENUM(BlueprintType)
enum class EWeaponState : uint8
{
	IDLE UMETA(DisplayName = "IDLE"),
	FIRE UMETA(DisplayName = "FIRE"), // 사격 중. 이 상태에서 Tick에서 FireRate에 따라 사격을 진행	
	AIM UMETA(DisplayName = "AIM"),
	RELOAD UMETA(DisplayName = "RELOAD"),
	ITEM UMETA(DisplayName = "ITEM")
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	NONE UMETA(DisplayName = "NONE"),
	AR UMETA(DisplayName = "AR"),
	SMG UMETA(DisplayName = "SMG"),
	SG UMETA(DisplayName = "SG"),	
	HG UMETA(DisplayName = "HG")
};

UENUM(BlueprintType)
enum class EWeaponReference : uint8
{
	NONE UMETA(DisplayName = "NONE"),
	AR_M4 UMETA(DisplayName = "AR_M4"),
	SMG_UMP45 UMETA(DisplayName = "SMG_UMP45")	
};

UENUM(BlueprintType)
enum class EWeaponFireMode : uint8
{
	SEMI_AUTO UMETA(DisplayName = "SEMI_AUTO"),
	FULL_AUTO UMETA(DisplayName = "FULL_AUTO"),	
	BURST UMETA(DisplayName = "BURST"),
};

USTRUCT(Atomic, BlueprintType)
struct FWeaponData : public FItemData
{
	GENERATED_USTRUCT_BODY()
	
public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Damage = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int RPM = 0;	

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int MaxAmmoInMagazine = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int BurstFireCount = 0;		

	UPROPERTY(EditAnywhere, BlueprintReadWrite) // 사격에 소모되는 탄수
	int RoundPerShot = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite) // 한 발에 발사되는 타격판정
	int PalletCount = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite) // 수평반동
	float HorizontalRecoil;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite) // 수직반동
	float VerticalRecoil;		

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EWeaponType WeaponType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString ReloadAnimPath;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString FireAnimPath;
};

USTRUCT(Atomic, BlueprintType)
struct FWeaponList
{
	GENERATED_USTRUCT_BODY()

	public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FWeaponData> WeaponArray;
};

