// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayableCharacter.h"

#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Table/WeaponDataStructure.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
APlayableCharacter::APlayableCharacter(const FObjectInitializer& ObjectInitializer)	
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	auto Capsule = GetCapsuleComponent();
	auto CurrentMesh = GetMesh();
	auto Arrow = GetArrowComponent();	
	auto Movement = GetMovementComponent();
		
	CloseSpring = CreateDefaultSubobject<USpringArmComponent>(TEXT("CLOSE_SPRING"));
	FarSpring = CreateDefaultSubobject<USpringArmComponent>(TEXT("FAR_SPRING"));
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("CAMERA"));
	

	RootComponent = Capsule;	
	Arrow->SetupAttachment(Capsule);
	CurrentMesh->SetupAttachment(Capsule);	
	
	CloseSpring->SetupAttachment(Capsule);
	FarSpring->SetupAttachment(CloseSpring);
	Camera->SetupAttachment(FarSpring);	

	CloseSpring->SetRelativeLocationAndRotation(FVector(0.f, 0.f, 100.f), FRotator(0.f, -90.f, 0.f));
	FarSpring->SetRelativeLocationAndRotation(FVector(0.f, 0.f, 0.f), FRotator(-5.f, 90.f, 0.f));	
}

// Called when the game starts or when spawned
void APlayableCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void APlayableCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();	
}

// Called every frame
void APlayableCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayableCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

const TArray<FWeaponData>& APlayableCharacter::GetWeaponList(const EItemType ItemType)
{
	FWeaponList* List = Inventory.Find(ItemType);	
	return List->WeaponArray;
}
