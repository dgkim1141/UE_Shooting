// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Table/ItemDataStructure.h"
#include "Table/WeaponDataStructure.h"
#include "PlayableCharacter.generated.h"

UCLASS(Blueprintable)
class APlayableCharacter : public ACharacter
{
	GENERATED_BODY()

public:// Sets default values for this character's properties
	APlayableCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void PostInitializeComponents() override;
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	UFUNCTION(BlueprintCallable)
	const TArray<FWeaponData>& GetWeaponList(const EItemType ItemType);

private:	
	UPROPERTY(Category = CharacterMesh, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* BaseScene;

	UPROPERTY(Category = CharacterMesh, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* MeshBase;

	UPROPERTY(Category = CharacterMesh, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CloseSpring;
	
	UPROPERTY(Category = CharacterMesh, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* FarSpring;

	UPROPERTY(Category = CharacterMesh, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* Camera;

	UPROPERTY(Category = Inventory, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TMap<EItemType, FWeaponList> Inventory;

	UPROPERTY(Category = Inventory, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TMap<EWeaponType, int> AmmoInventory;

	UPROPERTY(Category = Inventory, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TMap<EWeaponType, int> AmmoMax;

	UPROPERTY(Config)
	float TestValue = 0.5f;

	friend class APlayerDebugHUD;
};
