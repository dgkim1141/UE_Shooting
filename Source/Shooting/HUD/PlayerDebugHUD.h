// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "PlayerDebugHUD.generated.h"

/**
 * 
 */
UCLASS()
class APlayerDebugHUD : public AHUD
{
	GENERATED_BODY()
	
	APlayerDebugHUD();
public:
	virtual void DrawHUD() override;

	UFUNCTION(BlueprintCallable)
	void AddString(const FString& Title, const FText& Value);

	UFUNCTION(BlueprintCallable)
	void AddText(const FString& title, const FText& value);

	UFUNCTION(BlueprintCallable)
	void AddFloat(const FString& title, float value);
	
	UFUNCTION(BlueprintCallable)
	void AddInt	(const FString& title, int32 value);

	UFUNCTION(BlueprintCallable)
	void AddVector(const FString& title, FVector v);
	
	UFUNCTION(BlueprintCallable)
	void AddBool(const FString& title, bool value);

	// The horizontal offset to render the statistic values at.
	float HorizontalOffset = 150.0f;

private:

	// Convert a TCHAR pointer to FText.
	FText CStringToText(const TCHAR* text)
	{
		return FText::FromString(text);
	}

	// Convert a bool to FText.
	FText BoolToText(bool value)
	{
		return CStringToText((value == true) ? TEXT("true") : TEXT("false"));
	}


private:
	void RenderStatistic(const TCHAR* title, const FText& value, const FLinearColor& valueColor = FLinearColor::White);


	// Font used to render the debug information.
	UPROPERTY(Transient)
	UFont* MainFont = nullptr;

	// The current X coordinate.
	float X = 50.0f;

	// The current Y coordinate.
	float Y = 50.0f;

	// The line height to separate each HUD entry.
	float LineHeight = 16.0f;
};
