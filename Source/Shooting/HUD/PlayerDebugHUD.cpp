// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerDebugHUD.h"
#include "Engine/Canvas.h"
#include "CanvasItem.h"
#include "Shooting.h"

#include "UObject/ConstructorHelpers.h"
APlayerDebugHUD::APlayerDebugHUD()
{
	static ConstructorHelpers::FObjectFinder<UFont> Font(TEXT("/Engine/EngineFonts/Roboto"));

	if (Font.Succeeded())
	{
		MainFont = Font.Object;
	}
}

void APlayerDebugHUD::DrawHUD()
{
	Super::DrawHUD();
	X = Y = 50.f;


}


// Add a FText to the HUD for rendering.	

void APlayerDebugHUD::AddString(const FString & Title, const FText & Value)
{
	RenderStatistic(*Title, Value);
}

void APlayerDebugHUD::AddText(const FString& title, const FText & value)
{
	RenderStatistic(*title, value);
}

void APlayerDebugHUD::AddFloat(const FString& title, float value)
{
	RenderStatistic(*title, FText::AsNumber(value));
}

void APlayerDebugHUD::AddInt(const FString& title, int32 value)
{	
	RenderStatistic(*title, FText::AsNumber(value));
}

void APlayerDebugHUD::AddVector(const FString& title, FVector v)
{
	RenderStatistic(*title, FText::FromString(v.ToString()));
}

void APlayerDebugHUD::AddBool(const FString& title, bool value)
{
	RenderStatistic(*title, BoolToText(value), (value == false) ? FLinearColor::Red : FLinearColor::Green);
}

void APlayerDebugHUD::RenderStatistic(const TCHAR * title, const FText & value, const FLinearColor & valueColor)
{
	FCanvasTextItem item0(FVector2D(X, Y), CStringToText(title), MainFont, FLinearColor::White);
	item0.EnableShadow(FLinearColor(0.0f, 0.0f, 0.0f));
	Canvas->DrawItem(item0);
	FCanvasTextItem item1(FVector2D(X + HorizontalOffset, Y), value, MainFont, valueColor);
	item1.EnableShadow(FLinearColor(0.0f, 0.0f, 0.0f));
	Canvas->DrawItem(item1);
	Y += LineHeight;	
}
