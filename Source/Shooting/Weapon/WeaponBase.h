// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Table/WeaponDataStructure.h"
#include "WeaponBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDelegateOnFireWeapon);

UCLASS()
class AWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	virtual void PostInitializeComponents() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	UFUNCTION(BlueprintCallable)
	virtual void ChangeFireMode();

	UFUNCTION(BlueprintCallable)
	void PressFireInput(bool FirePressed);

	UFUNCTION(BlueprintCallable)
	void FireByMode(const EWeaponFireMode CurrentFireMode);	
	
	UFUNCTION(BlueprintCallable)
	bool PressReloadInput(); //returns character should play reload anim

	UFUNCTION(BlueprintCallable)
	const FString GetWeaponStateText();
		
	UFUNCTION(BlueprintCallable)
	void OnFinishReloadAnim();
private:
	UFUNCTION(Server, Reliable)
	void Server_OnFireWeapon();

	UFUNCTION(NetMulticast, Reliable)
	void Multicast_OnFireWeapon();	
	
protected:
	UPROPERTY(Category = Component, VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* RootScene;

	UPROPERTY(Category = Component, VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent* GunMesh;	

	UPROPERTY(Category = SocketName, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FName HandSocketName;

	UPROPERTY(Category = SocketName, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FName MuzzleSocketName;

	UPROPERTY(Category = Weapon, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FWeaponData WeaponData;
		
	UPROPERTY(Category = Weapon, EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TArray<EWeaponFireMode> FireModeList;	

	UPROPERTY(Category = Weapon, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	EWeaponFireMode CurrentFireMode = EWeaponFireMode::FULL_AUTO;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int CurrentAmmo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int CurrentAmmoInMagazine;

	UPROPERTY(Category = Weapon, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FVector FireDirection = {0.f, 0.f, 0.f};

	UPROPERTY(VisibleAnywhere, BlueprintAssignable)
	FDelegateOnFireWeapon OnFireWeapon;	

	UPROPERTY(Category = Weapon, EditAnywhere, BlueprintReadWrite)
	EWeaponState CurWeaponState = EWeaponState::IDLE;

	UPROPERTY(Category = WeaponAnimation, EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class UAnimationAsset* FireAnim = nullptr;

	//UPROPERTY(Category = WeaponAnimation, EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class UAnimationAsset* ReloadAnim = nullptr;

	float FireRate;
	int32 FireModeIdx = 0;	
private:
	/*bool IsWeaponFire = false;
	bool IsReloading = false;*/

	bool PressedFireInput = false;
	float FireDelay = 0.f;
	int32 BurstFireRemain = 0;

	
};
