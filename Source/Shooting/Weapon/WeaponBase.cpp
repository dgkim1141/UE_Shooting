// Fill out your copyright notice in the Description page of Project Settings.

#include "WeaponBase.h"
#include "Shooting.h"
#include "Components/SceneComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Animation/AnimInstance.h"

#include "DrawDebugHelpers.h"

// Sets default values
AWeaponBase::AWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootScene = CreateDefaultSubobject<USceneComponent>(TEXT("WeaponRoot"));
	GunMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Weapon"));
	GunMesh->SetRelativeRotation(FRotator(0.f, 0.f, 0.f));
	GunMesh->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	
	
	RootComponent = RootScene;
	GunMesh->SetupAttachment(RootScene);

	bNetLoadOnClient = true;
	bReplicates = true;
}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	
	if (FireModeList.Num() != 0)
	{
		CurrentFireMode = FireModeList[0];
	}

	float RoundPerSec = static_cast<float>(WeaponData.RPM) / 60.f;
	FireRate = 1.f / RoundPerSec;
}

void AWeaponBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);	
		
	if (CurWeaponState == EWeaponState::FIRE)
		FireByMode(CurrentFireMode);
}

void AWeaponBase::ChangeFireMode()
{
	if (FireModeList.Num() != 0)
	{
		FireModeIdx++;

		int32 ModeListNum = FireModeList.Num();
		FireModeIdx %= ModeListNum;

		CurrentFireMode = FireModeList[FireModeIdx];		
	}
}

void AWeaponBase::PressFireInput(bool _FirePressed)
{		
	PressedFireInput = _FirePressed;
	
	switch (CurrentFireMode)
	{
	case EWeaponFireMode::BURST:
		if (CurWeaponState == EWeaponState::IDLE && _FirePressed == true)
		{
			//LOG_SCREEN("BurstInput");				
			BurstFireRemain = FMath::Min(CurrentAmmoInMagazine, WeaponData.BurstFireCount);
			CurWeaponState = EWeaponState::FIRE;
		}
		else
		{
			//LOG_SCREEN("BurstInput %d", static_cast<int32>(WeaponState));
		}
		break;
	default:
		if (_FirePressed && CurWeaponState == EWeaponState::IDLE)
		{
			CurWeaponState = EWeaponState::FIRE;			
		}		
		else if (CurWeaponState != EWeaponState::RELOAD)
		{
			CurWeaponState = EWeaponState::IDLE;
		}

		FireDelay = 0.f;
		break;
	}
}

void AWeaponBase::FireByMode(const EWeaponFireMode InCurrentFireMode)
{	
	float DeltaTime = GetWorld()->GetDeltaSeconds();

	CurrentFireMode = InCurrentFireMode;
	switch (CurrentFireMode)
	{
	case EWeaponFireMode::FULL_AUTO:
		if (CurWeaponState == EWeaponState::FIRE)
		{
			if (FireDelay <= 0.f && CurrentAmmoInMagazine != 0)
			{				
				Server_OnFireWeapon();				
			}			

			FireDelay -= DeltaTime;
		}
		break;
	case EWeaponFireMode::SEMI_AUTO:
		if (CurWeaponState == EWeaponState::FIRE && CurrentAmmoInMagazine != 0)
		{					
			if (FireDelay <= 0.f && CurrentAmmoInMagazine != 0)
			{
				Server_OnFireWeapon();
				
				CurWeaponState = EWeaponState::IDLE;
			}

			FireDelay -= DeltaTime;
		}
		break;
	case EWeaponFireMode::BURST:
		if (BurstFireRemain != 0 && FireDelay <= 0.f)
		{
			BurstFireRemain--;			
			Server_OnFireWeapon();
		}
		else if (BurstFireRemain == 0 && FireDelay <= 0.f)
		{
			CurWeaponState = EWeaponState::IDLE;
		}
		

		FireDelay -= DeltaTime;
		break;

	default:
		break;
	}


}

bool AWeaponBase::PressReloadInput()
{		
	if (CurrentAmmoInMagazine == WeaponData.MaxAmmoInMagazine)
	{
		return false;
	}

	switch (CurWeaponState)
	{
	case EWeaponState::IDLE:	
		CurWeaponState = EWeaponState::RELOAD;	
		GunMesh->PlayAnimation(ReloadAnim, false);
		return true;		

	default:
		return false;
	}	
}

const FString AWeaponBase::GetWeaponStateText()
{
	switch (CurWeaponState)
	{
	case EWeaponState::IDLE:
		return TEXT("IDLE");
	case EWeaponState::FIRE:
		return TEXT("FIRE");
	/*case EWeaponState::FIRE_END:
		return TEXT("FIRE_END");*/
	case EWeaponState::AIM:
		return TEXT("AIM");
	case EWeaponState::RELOAD:
		return TEXT("RELOAD");
		
	default:
		return TEXT("STATE_ERR");
	}
}

void AWeaponBase::OnFinishReloadAnim()
{
	if (CurWeaponState == EWeaponState::RELOAD)
	{		
		CurWeaponState = EWeaponState::IDLE;
		if (PressedFireInput == true)
		{
			CurWeaponState = EWeaponState::FIRE;
		}

		if (CurrentAmmo != 0)
		{
			int NeedAmmo = WeaponData.MaxAmmoInMagazine - CurrentAmmoInMagazine;
			int ResultAmmo = FMath::Min(NeedAmmo, CurrentAmmo);

			if (CurrentFireMode == EWeaponFireMode::BURST)
			{
				BurstFireRemain = FMath::Min(ResultAmmo, WeaponData.BurstFireCount);
			}
			CurrentAmmo -= ResultAmmo;
			CurrentAmmoInMagazine += ResultAmmo;			

			//OnReloadComplete.Broadcast();
		}


	}
}

void AWeaponBase::Server_OnFireWeapon_Implementation()
{
	FireDelay = FireRate;
	CurrentAmmoInMagazine--;	
	if (CurrentAmmoInMagazine <= 0)
		CurWeaponState = EWeaponState::IDLE;		

	Multicast_OnFireWeapon();
}

void AWeaponBase::Multicast_OnFireWeapon_Implementation()
{	
	FTransform MuzzleTransform = GunMesh->GetSocketTransform(MuzzleSocketName);
	FVector DebugLineStart = MuzzleTransform.GetLocation();
	FVector DebugLineEnd = DebugLineStart + FVector(FireDirection * 2000.f);

	GunMesh->PlayAnimation(FireAnim, false);
	
	DrawDebugLine(GetWorld(), DebugLineStart, DebugLineEnd, FColor::Red, false, 0.3f, 0, 0.5f);
}